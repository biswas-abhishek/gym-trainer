
# Gym Trainer

### This is Simple Static Gym Trainer Site Build With **_[Foundation Css](https://get.foundation/)_** and **_[Foundation Panini Template](https://get.foundation/sites/docs/panini.html)_**

# Web Technologies I Used
![HTML](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/39da008e57dcebf7f5c3dcdad7b7b8674956385d/img/html.svg)![Sass](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/39da008e57dcebf7f5c3dcdad7b7b8674956385d/img/sass.svg)![Foundation CSS](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/39da008e57dcebf7f5c3dcdad7b7b8674956385d/img/zurb%20foundation.svg)
# Other Tools
![Gitlab](https://raw.githubusercontent.com/biswas-abhishek/biswas-abhishek/39da008e57dcebf7f5c3dcdad7b7b8674956385d/img/Gitlab.svg)

## Installation

To use this template, your computer needs:

- [NodeJS](https://nodejs.org/en/) (Version 12 or greater recommended)
- [Git](https://git-scm.com/)
- [Foundation CLI](https://www.npmjs.com/package/foundation-cli)
- [Bower](https://bower.io/)
- [Gulp](https://gulpjs.com/)
---
run :
```bash
npm start
```
Open Browser and `Enter` URL :
```
http://localhost:8000
```

To create compressed, production-ready assets, run `npm run build`.

