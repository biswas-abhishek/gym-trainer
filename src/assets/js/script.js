'use strict';
/*
 * Smooth Navigation When Page Scroll
 */
const anchorLink = document.querySelectorAll('.navbar-item');
const scrollSections = document.querySelectorAll('.scroll-nav-bar');
function scrollOnNavBar() {
  let scrollSection = scrollSections.length;
  while (
    --scrollSection &&
    window.scrollY + 97 < scrollSections[scrollSection].offsetTop
  ) {}
  anchorLink.forEach((ltx) => ltx.classList.remove('active'));
  anchorLink[scrollSection].classList.add('active');
}
scrollOnNavBar();
window.addEventListener('scroll', scrollOnNavBar);

/*
 * Transperant Background Of Navigation Bar
 */
window.addEventListener('scroll', function () {
  var desktopNavBar = document.querySelector('.top-bar');
  var phoneNavBar = document.querySelector('.title-bar');
  desktopNavBar.classList.toggle('sticky_nav', window.scrollY + 0);
  phoneNavBar.classList.toggle('sticky_nav', window.scrollY + 0);
});

/*
 * Footer Counter
 */
const counters = document.querySelectorAll('.counter');
window.addEventListener('scroll', function () {
  counters.forEach((counter) => {
    counter.innerText = '0';

    const updateCount = () => {
      const target = +counter.getAttribute('data-target'); // typeof Number otherwise String
      const count = +counter.innerText;
      const increment = target / 5;
      if (count < target) {
        counter.innerText = `${Math.ceil(count + increment)}+`;
        setTimeout(updateCount, 200);
      } else {
        counter.innerText = `${target}+`;
      }
    };
    updateCount();
  });
});
